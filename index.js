
// Retrieve all data 

//Array using map method


// fetch request (single todo list title only) =====================

	async function fetchData(){
		let result = await fetch('https://jsonplaceholder.typicode.com/todos')
		console.log(result)

		let json = await result.json()
		let arr = Object.values(json).map(item => item.title)
		console.log(arr);


	};

	fetchData();
	
// 5-6 GET Method (title and status) ============================
	fetch('https://jsonplaceholder.typicode.com/todos/3', {
		method: "GET" })
	.then((response) => response.json())
	.then((json) => console.log(`title is ${json.title} and the status is ${json.completed}`))



// 7-8 POST method =======================================
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "New title",
		"completed": true
	})
})
 .then((response) => response.json())
 .then((json) => console.log(json));



// 8-9 PUT method ===========================================
 fetch('https://jsonplaceholder.typicode.com/todos/2', {
 	method: "PUT",
 	headers:{
 		'Content-Type':'application/json'
 	},
 	body: JSON.stringify({
 		"title": "Updated title",
 		"description": "Updated Body",
 		"completed": false,
 		"dateCompleted": "Pending",
 		"userId": 1,
 		"id": 2
 	})
 })
 .then((response)=> response.json())
 .then((json) => console.log(json));



// 10-11 PATCH Method ==========================================
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"completed" : true,
		"dateCompleted": "20-21-21"
	})
})
 .then((response) => response.json())
 .then((json)=> console.log(json));

